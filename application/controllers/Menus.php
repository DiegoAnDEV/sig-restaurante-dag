<?php 
    class Menus extends CI_Controller
    {
        function __construct()
        {
        //siempre se debe poner el parent cuando hay herencia 
            parent::__construct();          
        }
        //Renderización de la vista que muestra los desayunos
        public function desayunos()
        {
            $this->load->view('header');
            $this->load->view('menus/desayunos');
            $this->load->view('footer');
        }
        public function almuerzos()
        {
            $this->load->view('header');
            $this->load->view('menus/almuerzos');
            $this->load->view('footer');
        }
        public function meriendas()
        {
            $this->load->view('header');
            $this->load->view('menus/meriendas');
            $this->load->view('footer');
        }
        public function cartas()
        {
            $this->load->view('header');
            $this->load->view('menus/cartas');
            $this->load->view('footer');
        }
        public function mision_vision()
        {
            $this->load->view('header');
            $this->load->view('menus/mision_vision');
            $this->load->view('footer');
        }
        public function contactos()
        {
            $this->load->view('header');
            $this->load->view('menus/contactos');
            $this->load->view('footer');
        }
    }
?>