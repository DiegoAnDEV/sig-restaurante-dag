<h1 style="text-align: center;">Bienvenidos a Picantería UTC</h1>
<div class="row">
	<div class="jumbotron bg-info col-md-12">
		<h1>El place de comer!</h1>
		<p>La picantería UTC abre sus puertas a su distinguidísima clientela para que puedan alimentarse de forma sana y deliciosa
		<br>	
		</p>
	</div>
</div>

<div class="row">
	<div class="col-md-6 ">
		<img src="<?php echo base_url();?>/assets/images/comedor.jpg" alt="">
	</div>
	<div class="col-md-6 ">
		<h2 class="text-center">Pensado para los estudiantes de la universidad</h2>
		<p class="text-justify">Difruta de variados platos preparados cuidadosamente para cada necesidad y cada momento del día
			<br>sin tener que preocuparte por los precios o la calidad del alimento.
			<br>Siempre estará garantizada la calidad!!
		</p>
	</div>
</div>