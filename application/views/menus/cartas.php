<h1 style="text-align: center;">Platos a la carta del día</h1>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/carta1.jpg " alt="..." height="100px">
        <div class="caption">
            <h3>Solomillo</h3>
            <p> Nuestro solomillo se prepara con un corte de carne de primera calidad, 
                tierno y jugoso, que se cocina a la perfección. Lo servimos acompañado de una deliciosa salsa de pimienta 
                que realza su sabor y le da un toque extra de distinción. 
                Este plato es ideal para aquellos que buscan disfrutar de una experiencia culinaria única y sabrosa. 
                ¡No se lo pierda!</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/carta2.webp " alt="...">
        <div class="caption">
            <h3>Seco de Chivo</h3>
            <p>Este delicioso platillo consiste en carne de chivo tierna y jugosa, 
                cocida a fuego lento con una combinación de especias tradicionales ecuatorianas. 
                La carne se sirve en un plato con una porción generosa de arroz blanco, frijoles, 
                plátanos maduros y una ensalada fresca de aguacate y tomate. 
                ¡Le invitamos a probarlo y disfrutar de los sabores auténticos de la cocina ecuatoriana!</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/carta3.jpg " alt="..." height="100px">
        <div class="caption">
            <h3>Chankonabe</h3>
            <p>Estofado que se elabora con una variedad de ingredientes, incluyendo pollo, pescado, verduras y fideos de arroz. 
                En nuestro restaurante, servimos el chankonabe en una olla caliente en la que se cocinan 
                los ingredientes deliciosos y sabrosos. 
                Es una opción única y nutritiva para aquellos que buscan una experiencia culinaria auténtica y satisfactoria</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
</div>