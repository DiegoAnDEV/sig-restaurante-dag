<h1 style="text-align: center;">Menú de almuerzos disponibles</h1>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/almu1.jpg " alt="..." height="100px">
        <div class="caption">
            <h3>Churrasco</h3>
            <p>Churrasco así a lo bien</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/almu2.jpg " alt="...">
        <div class="caption">
            <h3>Yahuarlocro</h3>
            <p>Lo que viene en el yahuarlocro</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/almu3.jpg " alt="..." height="100px">
        <div class="caption">
            <h3>Papipollo</h3>
            <p>Pollito con papas</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
</div>