<h1 style="text-align: center;">Misión y Visión</h1>

<div class="row">
	<div class="col-md-6 ">
        <h2 class="text-center">Misión</h2>
		<p class="text-justify">
        En nuestro restaurante para estudiantes universitarios, nuestra misión es proporcionar alimentos nutritivos y sabrosos a precios asequibles para satisfacer las necesidades de nuestros clientes. Queremos crear un ambiente cómodo y acogedor donde los estudiantes puedan disfrutar de una experiencia culinaria única y satisfactoria. 
		</p>
	</div>
	<div class="col-md-6 ">
		<h2 class="text-center">Visión</h2>
		<p class="text-justify">Nuestra visión es convertirnos en el restaurante preferido de los estudiantes universitarios, ofreciendo opciones saludables y deliciosas que se adapten a sus necesidades y estilo de vida. Nos esforzamos por utilizar ingredientes frescos y locales siempre que sea posible, y por ofrecer un servicio excepcional en todo momento.
		</p>
	</div>
</div>
