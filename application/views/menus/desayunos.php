<h1 style="text-align: center;">Menú de desayunos disponibles</h1>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/desa1.png " alt="..." height="100px">
        <div class="caption">
            <h3>Desayuno Continental</h3>
            <p>Compuesto por Café, jugo de fruta, pan, fruta fresca y galletas</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/desa2.png " alt="..." height="100px">
        <div class="caption">
            <h3>Desayuno Costeño</h3>
            <p>Compuesto por Café, Jugo de fruta, Tigrillo, Bolones de chicharrón o queso, huevos fritos y pan (opcional)</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="thumbnail">
        <img src="<?php echo base_url();?>/assets/images/desa3.jpg " alt="..." height="100px">
        <div class="caption">
            <h3>Desayuno de Campeones</h3>
            <p>Energía, proteína y cafeína en cada bocado. :-)</p>
            <p><a href="#" class="btn btn-primary" role="button">Ordenar</a> </p>
        </div>
        </div>
    </div>
</div>