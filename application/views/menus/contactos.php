<h1 style="text-align: center;">Contáctenos</h1>
<div class="row">
    <div class="col-md-4">
        <h2 class="text-center">Correos</h2>
        <p class="text-center">
            comedoruniver@utc.edu.ec
            <br>picanteriaUTC@gmail.com
        </p>
    </div>
    <div class="col-md-4">
        <h2 class="text-center">Teléfonos</h2>
        <p class="text-center">
            (02)2346-123
            <br>09463718294
            <br>09898765645
        </p>
    </div>
    <div class="col-md-4">
        <h2 class="text-center">Dirección</h2>
        <p class="text-center">
            Av. Simón Rodríguez, Edificio B, Segundo Piso
            <br>07:00-19:00
        </p>
    </div>
</div>
<hr>
<h2 class="text-center">Ubicación</h2>
<div class="row">
    <div class="col">
        <div id="mapaDireccion" style="width: 100%; height: 700px;"></div>
    </div>
</div>

<!--Script: Generación del mapa usando la api de google para integrarlo en el div mapaDireccion-->
<script type="text/javascript">
        function initMap()
        {
            //Creando el punto central del mapa
            var coordenadaCentral=
            new google.maps.LatLng(-0.9178940285381498, -78.63296922564011);
            
            var mapa1 =
            new google.maps.Map(document.getElementById('mapaDireccion'),
            {
                center:coordenadaCentral,
                zoom:20,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            }
            );
        }
</script>
